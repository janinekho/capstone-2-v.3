let registerForm= document.querySelector('#registerUser');

//action(event) and call back function(anong gagawin niya)
//why e means event
//event{
	/*property1:values
}*/
registerForm.addEventListener("submit",(e) =>{
    e.preventDefault()//prevents input forms to revert to default/blank value
    let firstName = document.querySelector("#firstName").value;
    let lastName = document.querySelector("#lastName").value;
    let mobileNumber = document.querySelector("#mobileNumber").value;
    let userEmail = document.querySelector("#userEmail").value;
    let password1 = document.querySelector("#password1").value;
    let password2 = document.querySelector("#password2").value;

    if((password1 !== '' && password2 !== '') && (password1=== password2)&& (mobileNumber.length === 11)){
       //fetch('url',{options}) //options is actually an obkect
       //to process a request para siyang si postman
    	fetch('https://thawing-reaches-86091.herokuapp.com/api/users/email-exists', {//key value pairs
    		method: 'POST',
    		headers: {
    			'Content-Type': 'application/json'
    		},
    		//concerting JSON file to string yun kasi na babasa ng fetch request
    		body: JSON.stringify({
    			email: userEmail
    		})
    	}) //chaining multiple functions
    	//backend from controller> user.js return resultFromFind.length > 0 ? true : false
	    //res is yung true or false function
    	.then(res => res.json())
    	.then(data =>{
    		console.log(data);

    		if(data === false){
               //Registering a User
    			fetch('https://thawing-reaches-86091.herokuapp.com/api/users', {
    				method: 'POST',
    				headers: {
    					'Content-Type':'application/json'
                    
    				},
    				body: JSON.stringify({
    					firstName: firstName,
    					lastName: lastName,
    					email: userEmail,
    					password: password1,
    					mobileNo: mobileNumber
    				})
    			})
                .then(res => res.json())
                .then(data => {
                	console.log(data)
                    //login page
                	if (data === true){
                		alert("registered successfully");
                		window.location.replace("login.html");
                	} else {
                		alert("something went wrong");
                	}
                })
    		} else {
    			alert("Email has been used. Try a different one.")
    		}
    	})

    } else {
    	alert("something went wrong")
    }
})

