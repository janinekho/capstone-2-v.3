//retrieve the user information for isAdmin
let adminUser = localStorage.getItem("isAdmin");
console.log(adminUser);
let token = localStorage.getItem('token');

//will contain the html for the different buttons per user
let cardFooter; // why does it not have a value. This is where we'll store diff buttons for diff users


fetch('https://thawing-reaches-86091.herokuapp.com/api/courses', {
//no headers //no body(wala naman tayong pinapasahan)
 method: 'GET',
 headers: {
 	'Authorization': `Bearer ${token}`
 }
})
.then(res => res.json())
.then(data => {
console.log(data);
console.log(token);
if(token == null){
  alert('Please login first!')
  window.location.replace('login.html');
  
}
    // Variable to store the card/message to show if there's no courses
	let courseData;

		if(data.length < 1) {
		courseData = "No courses available"
		} else{

		courseData = data.map(course => { //map function returns an array.

			console.log(course._id);

			//logic for rendering different buttons based on the user

			if(adminUser === "false" || !adminUser) { // not an admin
				//? < if you want to pass a URL // pass data via URL
				//Format: relative_path?parameter_name=value&param_name2
				//./course.html?courseId=${course._id}
				cardFooter =
				`
                 <a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>


				`
              
            /*console.log(data.isActive);
             if(data.isActive === true){

             	cardFooter=

             	`
               <a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} button id="disableButton onclick= disable(${course._id})" class="btn btn-danger text-white btn-block dangerButton"> Disable Course </a>
                 `

             }
           */

             

			/*} else { //admin
				cardFooter = 
				`
				<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-secondary text-white btn-block secondaryButton">View Enrollees </a>
                <a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Edit </a>
	             
	             


				`*/
			} else { //admin
                cardFooter = 
                `
                
                <a href="./course.html?courseId=${course._id}" value=${course._id}  id="viewButton" class="btn btn-secondary text-white btn-block secondaryButton">View Enrollees </a>
                <a href="./editCourse.html?courseId=${course._id}" value=${course._id} id="editButton" class="btn btn-primary text-white btn-block editButton"> Edit </a>
                `

                let isActive = course.isActive;
                if(isActive){
                    cardFooter = cardFooter +
                    ` <button id= '${course._id}' value= 1 onclick="enableDisable('${course._id}',this.value)"  id= "enableButton" class="btn btn-danger text-white btn-block dangerButton"> Enable Course </button> 
                    `

                } else {
                    cardFooter = cardFooter + 
                    `
                    <button id= '${course._id}' value= 0 onclick="enableDisable('${course._id}',this.value)" id="disableButton" class="btn btn-danger text-white btn-block warningButton"> Disable Course </button> 
                    `
                }
            }
            	
             
			/*}*/

			return (
				//what happens is there will be an array of cards

                   `

                    <div class="col-md-6 my-3">
		                <div class='card'>
		                    <div class='card-body'>
		                        <h5 class='card-title'>${course.name}</h5>
		                            <p class='card-text text-left'>
		                                    ${course.description}
		                            </p>
		                            <p class='card-text text-right'>
		                                   ₱ ${course.price}
		                            </p>

		                            </div>
		                            <div class='card-footer'>
		                                ${cardFooter}
		                            </div>
		                        </div>
		                    </div>

                   `



				)

		}).join("") 

		let coursesContainer = document.querySelector("#coursesContainer");

		coursesContainer.innerHTML = courseData;

	}
})


let modalButton = document.querySelector('#adminButton');

	if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null;
	} else {
		modalButton.innerHTML =
			`
   			<div class="col-md-2 offset-md-10 col-sm-4 ">
			<a href="./addCourse.html" id="addCoursebtn" class="btn btn-block btn-primary"> + Add Course </a>
			</div>


			`

}



function enableDisable(courseId, isActive){
	console.log(isActive);
    let button= document.getElementById(courseId);
    if(isActive== 1){
        // disable it
        fetch(`https://thawing-reaches-86091.herokuapp.com/api/courses/${courseId}`,{
            method: 'PUT',
                   headers: {
                   'Authorization': `Bearer ${token}`
                  }
        
          })
        .then(res => res.json())
        .then(data => {
        	console.log(data);
      
        })

        button.innerHTML = `Disable Course`;
        button.innerText= `Disable Course`;
        button.value = 0;
    } 

    else { // enable it
    	fetch(`https://thawing-reaches-86091.herokuapp.com/api/courses/${courseId}`,{
            method: 'DELETE',
                   headers: {
                   'Authorization': `Bearer ${token}`
                  }
        
          })
        .then(res => res.json())
        .then(data => {
        	console.log(data);
          // do nothing
        })



        button.innerHTML = `Enable Course`;
        button.innerText = `Enable Course`;

        button.value = 1;
        alert('Successfully disabled');
    }



}






